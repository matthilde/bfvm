BFVM by matthilde

This is an optimized brainfuck interpreter that uses a VM with an instruction
set designed to run brainfuck.
The assembler includes a few optimizations to improve speed greatly.
And it now includes a Just-In-Time compiler (JIT) that is even faster.

NOTE: The JIT has been designed to run on x86-64 CPUs, if you have something
      else, the JIT will not work.
      And since it uses mmap, it only works for Linux.
      However nothing prevents you to port this interpreter, if it's even worth
      it...

FILES
=====

 bfvm.c        - The brainfuck interpreter, code is commented
 naive.c       - A naive brainfuck interpreter I wrote a year ago
 benchmark.txt - The benchmarks
 hello.bf      - Hello World
 mandelbrot.bf - A copy of the mandelbrot fractals algorithm in brainfuck

COMPILING
=========

Compiling the brainfuck interpreter
 
 cc bfvm.c -O3 -o bfvm

Compiling the naive brainfuck interpreter

 cc naive.c -o naive

USAGE
=====

 bfvm [-vjc] FILENAME
  -v is to enable verbose logs, used to debug the assembler
  -j is to enable JIT mode
  -c is to enable compiler mode (will output x86 code)

 naive FILENAME

LICENSE
=======

 Look, I don't care about this piece of software. Do whatever you want with it
 tbh, belongs to public domain i guess

