#include <stdio.h>
#define BUFSIZE 1000000
// Brainfuck interpreter in C
// Iteration 2

// The brainfuck interpreter now reads actual files.
// usage : bf [filename]
// I did that with my poor C skills.

int main(int argc, char** argv)
{
    if (argc != 2) return 1;

    char     buffer[BUFSIZE], data[BUFSIZE];
    int     i, c;
    unsigned int b, j;
    FILE* f;
    j = 0;

    f = fopen(argv[1], "r");
    if (!f) return 1;

    for (i = 0; i < BUFSIZE; ++i)
        data[i] = 0;
    for (i = 0; (c=fgetc(f)) != EOF && i < BUFSIZE; ++i)
    {
        switch (c)
        {
            case',':case '.':case '<':case '>':case '+':case '-':case '[':case ']':
                buffer[j] = c;
                ++j;
                break;
        }
    }
    buffer[j] = '\0';
    i = 0;

    while ((c=buffer[i]) != '\0')
    {
        switch (c)
        {
            case '>':
                ++j;
                break;
            case '<':
                --j;
                break;
            case '+':
                ++data[j];
                break;
            case '-':
                --data[j];
                break;
            case '.':
                putchar(data[j]);
                break;
            case ',':
                data[j] = getchar();
                break;
            case '[':
                if (data[j] == 0)
                {
                    b = 1;
                    while (b!=0)
                    {
                        ++i;
                        switch ((c=buffer[i]))
                        {
                            case '[':
                                ++b;
                                break;
                            case ']':
                                --b;
                                break;
                        }
                    }
                }
                break;
            case ']':
                if (data[j] != 0)
                {
                    b = 1;
                    while (b!=0)
                    {
                        --i;
                        switch ((c=buffer[i]))
                        {
                            case ']':
                                ++b;
                                break;
                            case '[':
                                --b;
                                break;
                        }
                    }
                }
                break;
        }

        ++i;
    }
}
